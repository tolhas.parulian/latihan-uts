public class Quiz3_2{
	UserRepository userRepository;
	PasswordValidator passwordValidator;
	
	public AuthenticationService(UserRepository userRepository, PasswordValidator passwordValidator){
		this.userRepository = userRepository;
		this.passwordValidator = passwordValidator;
	}
	
	public void getUserByEmail(String email){
		if(userRepository.getUserByEmail(email) == null)
			throw new AuthenticationException();
	}
	
	public void getUserByUsername(String username){
		if(userRepository.getUserByUsername(username) == null)
			throw new AuthenticationException();
	}
	
	public void passwordSimilarWithEmail(String password, String email){
		if(passwordValidator.isSimilarWithEmail(password, email) == null)
			throw new AuthenticationException();
	}
	
	public void passwordSimilarWithUsername(String password, String username){
		if(passwordValidator.isSimilarWithEmail(password, username) == null)
			throw new AuthenticationException();
	}
	
	public static void register(String username, String email, String password, String name) throws AuthenticationException{
		getUserByEmail(email);
		getUserByUsername(username);
		passwordSimilarWithEmail(password, email);
		passwordSimilarWithUsername(password, username);
		
		try{
			userRepository.createUser(username, name.toUpperCase(), password, email);
		}
		catch(Exception anException){
			System.out.println("Something went wrong!");
		}
	}
}